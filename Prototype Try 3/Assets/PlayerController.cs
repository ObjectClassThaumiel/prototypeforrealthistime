﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
 
{

    private Rigidbody _RB;

    public int speed;
    public Text scoreText;
    private int count;

  
    void Start()
    {
        _RB = GetComponent<Rigidbody>();
       
    }

    private void FixedUpdate()
    {
        float moveVertical = Input.GetAxis("Vertical");
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
        _RB.AddForce(movement * speed);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(true);
            


        }
    }

}
